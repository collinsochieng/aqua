<?php

use app\helpers\MyKartikGridView;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
rmrevin\yii\fontawesome\AssetBundle::register($this);


$this->title = 'Audit Report';
?>


<div class="row">

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <!--            <h4><i class="icon fa fa-check"></i>Saved!</h4>-->
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <!--            <h4><i class="icon fa fa-check"></i>Saved!</h4>-->
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <div >

        <?= app\helpers\MyKartikGridView::widget([
            'dataProvider' => $dataProvider,
            'searchField' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                ],
                [
                    'label' => 'User Action',
                    'value' => 'user_action',
                ],
                [
                    'label' => 'Action Description',
                    'value' => 'description_action',
                ],
                [
                    'label' => 'Table Affected',
                    'value' => 'table_affected',

                ],
                [
                    'label' => 'Date',
                    'value' => 'date_log',
                ],
                [
                    'label' => 'Username(Email Address)',
                    'value' => 'user.email_address',
                ],

            ],

            'exportConfig' => [
                MyKartikGridView::CSV => ['label' => 'Export as CSV', 'filename' => 'Audit Report -' . date('d-M-Y')],
                MyKartikGridView::HTML => ['label' => 'Export as HTML', 'filename' => 'Audit Report -' . date('d-M-Y')],
                MyKartikGridView::PDF => ['label' => 'Export as PDF', 'filename' => 'Audit Report-' . date('d-M-Y')],
                MyKartikGridView::EXCEL => ['label' => 'Export as EXCEL', 'filename' => 'Audit Report -' . date('d-M-Y')],
                MyKartikGridView::TEXT => ['label' => 'Export as TEXT', 'filename' => 'Audit Report -' . date('d-M-Y')],
            ],

        ]); ?>
    </div>
</div>