<?php

namespace app\controllers;

use app\models\AuditReport;
use app\models\CartProducts;
use app\models\OrderProducts;
use app\models\Products;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use app\helpers\ConditonBuilder;
use app\helpers\CrudConstants;

class ProductsController extends  SiteController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['products'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['savecart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['countcart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['audit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['cartitems','deleteitem','orderitems'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProducts()
    {

        $model=new Products();
        $AuditLog=new AuditReport();
        $AuditLog->user_action=$AuditLog::R;
        $AuditLog->description_action='View Products Available';
        $AuditLog->table_affected=Products::getTableSchema()->name;
        $AuditLog->user_id=\Yii::$app->user->identity->user_id;
        $AuditLog->save(false);

        $ter=$model::find()->asArray()->all();



        return $this->render('products', [
            'model' => $ter,
        ]);
    }

    public function actionSavecart()
    {
        $model = new CartProducts();

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
        if (Yii::$app->request->isAjax ) {
            $validate=$model::find()->where(['product_id'=>$_POST['product_id']])->all();
          //  var_dump($_POST['product_id']);die;
            if($validate) {
                echo 'Item already added to cart: ID=> '.$_POST['product_id'];
                exit;
            }else
            $model->user_id=Yii::$app->user->identity->user_id;
            $model->product_id=$_POST['product_id'];
            $model->product_name=$_POST['product_name'];
            $model->product_price=$_POST['product_price'];
            $model->product_code=$_POST['product_code'];
            $model->product_size=$_POST['product_size'];
            $model->save(false);
            $transaction->commit();
         echo  "Data inserted";
        }}
        catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    public function actionCountcart(){

        if (Yii::$app->request->isAjax) {
            $user_id = Yii::$app->user->identity->user_id;
            $model = new CartProducts();
            $result = $model::find()->where(['user_id' => $user_id])->count();

            return $result;
        }

    }


    public function actionCartitems()
    {    $model = new CartProducts();
        $condition = new ConditonBuilder();
        $gt = Yii::$app->user->identity->user_id;
        $conditionrt = $model::find()->where(['user_id' => $gt])->asArray()->all();
        $condition->addCondition('user_id IN(select user_id from cart_products where user_id=:id)', [':id' => $gt]);
        // var_dump($condition); exit;




        if (Yii::$app->request->post('hasEditable')) {
            // instantiate your book model for saving
            $bookId = Yii::$app->request->post('editableKey');
            $model = CartProducts::findOne($bookId);

            // store a default json response as desired by editable
            $out = Json::encode(['output'=>'', 'message'=>'']);

            // fetch the first entry in posted data (there should only be one entry
            // anyway in this array for an editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $posted = current($_POST['CartProducts']);
            $post = ['CartProducts' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one
                // EditableColumn in the grid view. We evaluate here a
                // check to see if buy_amount was posted for the Book model
                if (isset($posted['quantity'])) {
                    $output = Yii::$app->formatter->asInteger($model->quantity);
                }

                // similarly you can check if the name attribute was posted as well
                // if (isset($posted['name'])) {
                // $output = ''; // process as you need
                // }
                $out = Json::encode(['output'=>$output, 'message'=>'']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }




        return $this->render('cartitems',
            array('dataProvider'=> CartProducts::searchModel(10,'cart_id',$condition),
                 'model'=>$model,
                ));
    }


    public function actionDeleteitem($product_id){


        $rem= new CartProducts();
        $Auditlog=new AuditReport();
        $countrem=$rem::find()->where(['user_id'=>\Yii::$app->user->identity->user_id])->count();
        $connection= \Yii::$app->db;
        $transaction=$connection->beginTransaction();
        $model=CartProducts::find()->where(['cart_product_id'=>$product_id])->one();
        if($model){
            $model->delete();
            \Yii::$app->session->setFlash('success','Item Removed');
            //log action into Audit Table
            $Auditlog->user_action=$Auditlog::D;
             $Auditlog->description_action='Removed Item From Cart';
             $Auditlog->table_affected=CartProducts::getTableSchema()->name;
             $Auditlog->user_id=\Yii::$app->user->identity->user_id;
             $Auditlog->save(false);
            $transaction->commit();

            if($countrem >= 1){

            return   $this->redirect(['products/cartitems']);
            }else{
           return  $this->redirect(['products/products']);
            \Yii::$app->session->setFlash('error','Dear user select products items');}

        }else{
            $transaction->rollBack();
        \Yii::$app->session->setFlash('error','Item cannot be removed');
      return $this->redirect(['products/cartitems']);}

    }
    public function actionOrderitems($user_id){
        $model=CartProducts::find()->where(['user_id'=>$user_id])->asArray()->all();
        $final_price=0;

            foreach ($model as $fr => $value){
               // var_dump(count($model)); exit;
                $final_price +=$value['product_price']*$value['cart_quantity'];
            }

        $final=new OrderProducts();
        if($model){
            $connection= \Yii::$app->db;
            $transaction=$connection->beginTransaction();
            try{
           if($final->load(\Yii::$app->request->post())){

               foreach ($model as $fr => $value){
               $final->product_id=$value['product_id'];
              // $final->county_name=$_POST['Location']['county_name'];
               $final->order_product_code=$value['order_product_code'];
               $final->order_product_name=$value['order_product_name'];
               $final->order_product_size=$value['order_product_size'];
               $final->order_quantity=$value['cart_quantity'];
               $final->product_price=$final_price;
               $final->user_id=$user_id;
               $final->save(false);
               $transaction->commit();
               return $this->redirect(['products/products']);}

           }} Catch (\Exception $e){
                $transaction->rollBack();
                \Yii::$app->session->setFlash('error', 'Abort Abort!!! Immediate evac Co-ordinates Alpha 10 Bravo 50 Tango Down'. $e );
                return $this->redirect(['products/cartitems']);
            }

        }

   return $this->render('orderitems',['model'=>$final,'dr'=>$final_price]);

    }

    public function actionAudit(){

        $model=new AuditReport();
        $condition = new ConditonBuilder();
        $gt = Yii::$app->user->identity->user_id;
       // $conditionrt = $model::find()->where(['user_id' => $gt])->asArray()->all();
        $condition->addCondition('user_id IN(select user_id from audit_report )', [':id' => $gt]);






        return $this->render('audit',
            array('dataProvider'=> AuditReport::searchModel(10,'audit_id',$condition),
                'model'=>$model,
            ));

    }


}
