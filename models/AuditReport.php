<?php

namespace app\models;

use app\helpers\MyActiveRecord;
use Yii;

/**
 * This is the model class for table "audit_report".
 *
 * @property int $audit_id
 * @property string $user_action
 * @property string $description_action
 * @property string $table_affected
 * @property string $date_log
 * @property int $user_id
 *
 * @property Users $user
 */
class AuditReport extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    Const C='CREATE';
    Const R='READ';
    Const U='UPDATE';
    Const D='DELETE';





    public static function tableName()
    {
        return 'audit_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_action', 'table_affected', 'user_id'], 'required'],
            [['date_log'], 'safe'],
            [['user_id'], 'integer'],
            [['user_action'], 'string', 'max' => 15],
            [['description_action', 'table_affected'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'audit_id' => 'Audit ID',
            'user_action' => 'User Action',
            'description_action' => 'Description Action',
            'table_affected' => 'Table Affected',
            'date_log' => 'Date Log',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
