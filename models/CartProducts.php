<?php

namespace app\models;

use app\helpers\MyActiveRecord;
use Yii;

/**
 * This is the model class for table "cart_products".
 *
 * @property int $cart_id
 * @property int $user_id
 * @property int $cart_product_id
 * @property string $cart_product_code
 * @property string $cart_product_name
 * @property string $cart_product_size
 * @property int $product_price
 * @property int $cart_quantity
 *
 * @property Users $user
 */
class CartProducts extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_price'], 'required'],
            [['user_id', 'cart_product_id', 'product_price', 'cart_quantity'], 'integer'],
            [['cart_product_code'], 'string', 'max' => 100],
            [['cart_product_name'], 'string', 'max' => 20],
            [['cart_product_size'], 'string', 'max' => 10],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cart_id' => 'Cart ID',
            'user_id' => 'User ID',
            'cart_product_id' => 'Cart Product ID',
            'cart_product_code' => 'Cart Product Code',
            'cart_product_name' => 'Cart Product Name',
            'cart_product_size' => 'Cart Product Size',
            'product_price' => 'Product Price',
            'cart_quantity' => 'Cart Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
