<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_products".
 *
 * @property int $order_id
 * @property string $county_name
 * @property int $order_product_id
 * @property string $order_product_code
 * @property string $order_product_name
 * @property string $order_product_size
 * @property string $location_description
 * @property int $order_quantity
 * @property int $order_product_price
 * @property int $phone_number
 * @property int $user_id
 *
 * @property Location $countyName
 * @property Users $user
 */
class OrderProducts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county_name', 'location_description', 'phone_number', 'user_id'], 'required'],
            [['order_product_id', 'order_quantity', 'order_product_price', 'phone_number', 'user_id'], 'integer'],
            [['county_name', 'order_product_code', 'order_product_name'], 'string', 'max' => 100],
            [['order_product_size', 'location_description'], 'string', 'max' => 20],
            [['county_name'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['county_name' => 'county_name']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'county_name' => 'County Name',
            'order_product_id' => 'Order Product ID',
            'order_product_code' => 'Order Product Code',
            'order_product_name' => 'Order Product Name',
            'order_product_size' => 'Order Product Size',
            'location_description' => 'Location Description',
            'order_quantity' => 'Order Quantity',
            'order_product_price' => 'Order Product Price',
            'phone_number' => 'Phone Number',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountyName()
    {
        return $this->hasOne(Location::className(), ['county_name' => 'county_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
